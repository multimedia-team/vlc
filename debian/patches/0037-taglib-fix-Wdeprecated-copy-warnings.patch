From: Alexandre Janniaux <ajanni@videolabs.io>
Date: Fri, 7 Feb 2020 23:50:00 +0100
Subject: taglib: fix -Wdeprecated-copy warnings
MIME-Version: 1.0
Content-Type: text/plain; charset="utf-8"
Content-Transfer-Encoding: 8bit

Warnings were happening because we were using copy operator= as the
assigned object was already initialized. This patch use the copy
constructor or move constructor instead if available.

Those warnings were:

../../modules/meta_engine/taglib.cpp: In function ‘void ReadMetaFromXiph(TagLib::Ogg::XiphComment*, demux_meta_t*, vlc_meta_t*)’:
../../modules/meta_engine/taglib.cpp:672:39: warning: implicitly-declared ‘TagLib::StringList& TagLib::StringList::operator=(const TagLib::StringList&)’ is deprecated [-Wdeprecated-copy]
  672 |     list = tag->fieldListMap()[keyName];                                       \
      |                                       ^
../../modules/meta_engine/taglib.cpp:681:5: note: in expansion of macro ‘SET’
  681 |     SET( "COPYRIGHT", Copyright );
      |     ^~~
In file included from /usr/include/taglib/fileref.h:30,
                 from ../../modules/meta_engine/taglib.cpp:58:
/usr/include/taglib/tstringlist.h:59:5: note: because ‘TagLib::StringList’ has user-provided ‘TagLib::StringList::StringList(const TagLib::StringList&)’
   59 |     StringList(const StringList &l);
      |     ^~~~~~~~~~

Signed-off-by: Jean-Baptiste Kempf <jb@videolan.org>
(cherry picked from commit be4f907a0e834c10277b8217520f745238da1c13)
---
 modules/meta_engine/taglib.cpp | 51 +++++++++++++++++++++++++-----------------
 1 file changed, 30 insertions(+), 21 deletions(-)

diff --git a/modules/meta_engine/taglib.cpp b/modules/meta_engine/taglib.cpp
index 31ab839..2ef0593 100644
--- a/modules/meta_engine/taglib.cpp
+++ b/modules/meta_engine/taglib.cpp
@@ -703,17 +703,20 @@ static void ReadMetaFromId3v2( ID3v2::Tag* tag, demux_meta_t* p_demux_meta, vlc_
  */
 static void ReadMetaFromXiph( Ogg::XiphComment* tag, demux_meta_t* p_demux_meta, vlc_meta_t* p_meta )
 {
-    StringList list;
     bool hasTrackTotal = false;
-#define SET( keyName, metaName )                                               \
-    list = tag->fieldListMap()[keyName];                                       \
-    if( !list.isEmpty() )                                                      \
-        vlc_meta_Set##metaName( p_meta, (*list.begin()).toCString( true ) );
+#define SET( keyName, metaName ) \
+    { \
+        StringList tmp_list { tag->fieldListMap()[keyName] }; \
+        if( !tmp_list.isEmpty() ) \
+            vlc_meta_Set##metaName( p_meta, (*tmp_list.begin()).toCString( true ) ); \
+    }
 
 #define SET_EXTRA( keyName, metaName ) \
-    list = tag->fieldListMap()[keyName]; \
-    if( !list.isEmpty() ) \
-        vlc_meta_AddExtra( p_meta, keyName, (*list.begin()).toCString( true ) );
+    { \
+        StringList tmp_list = tag->fieldListMap()[keyName]; \
+        if( !tmp_list.isEmpty() ) \
+            vlc_meta_AddExtra( p_meta, keyName, (*tmp_list.begin()).toCString( true ) ); \
+    }
 
     SET( "COPYRIGHT", Copyright );
     SET( "ORGANIZATION", Publisher );
@@ -729,25 +732,31 @@ static void ReadMetaFromXiph( Ogg::XiphComment* tag, demux_meta_t* p_demux_meta,
 #undef SET
 #undef SET_EXTRA
 
-    list = tag->fieldListMap()["TRACKNUMBER"];
-    if( !list.isEmpty() )
+    StringList track_number_list = tag->fieldListMap()["TRACKNUMBER"];
+    if( !track_number_list.isEmpty() )
     {
-        int i_values = ExtractCoupleNumberValues( p_meta, (*list.begin()).toCString( true ),
+        int i_values = ExtractCoupleNumberValues( p_meta, (*track_number_list.begin()).toCString( true ),
                  vlc_meta_TrackNumber, vlc_meta_TrackTotal );
         hasTrackTotal = i_values == 2;
     }
     if( !hasTrackTotal )
     {
-        list = tag->fieldListMap()["TRACKTOTAL"];
-        if( list.isEmpty() )
-            list = tag->fieldListMap()["TOTALTRACKS"];
-        if( !list.isEmpty() )
-            vlc_meta_SetTrackTotal( p_meta, (*list.begin()).toCString( true ) );
+        StringList track_total_list { tag->fieldListMap()["TRACKTOTAL"] };
+        if( track_total_list.isEmpty() )
+        {
+            StringList total_tracks_list { tag->fieldListMap()["TOTALTRACKS"] };
+            if( !total_tracks_list.isEmpty() )
+                vlc_meta_SetTrackTotal( p_meta, (*total_tracks_list.begin()).toCString( true ) );
+        }
+        else
+        {
+            vlc_meta_SetTrackTotal( p_meta, (*track_total_list.begin()).toCString( true ) );
+        }
     }
 
     // Try now to get embedded art
-    StringList mime_list = tag->fieldListMap()[ "COVERARTMIME" ];
-    StringList art_list = tag->fieldListMap()[ "COVERART" ];
+    StringList mime_list { tag->fieldListMap()[ "COVERARTMIME" ] };
+    StringList art_list { tag->fieldListMap()[ "COVERART" ] };
 
     input_attachment_t *p_attachment;
 
@@ -774,14 +783,14 @@ static void ReadMetaFromXiph( Ogg::XiphComment* tag, demux_meta_t* p_demux_meta,
     }
     else
     {
-        art_list = tag->fieldListMap()[ "METADATA_BLOCK_PICTURE" ];
-        if( art_list.size() == 0 )
+        StringList block_picture_list { tag->fieldListMap()[ "METADATA_BLOCK_PICTURE" ] };
+        if( block_picture_list.size() == 0 )
             return;
 
         uint8_t *p_data;
         int i_cover_score;
         int i_cover_idx;
-        int i_data = vlc_b64_decode_binary( &p_data, art_list[0].toCString(true) );
+        int i_data = vlc_b64_decode_binary( &p_data, block_picture_list[0].toCString(true) );
         i_cover_score = i_cover_idx = 0;
         /* TODO: Use i_cover_score / i_cover_idx to select the picture. */
         p_attachment = ParseFlacPicture( p_data, i_data, 0,

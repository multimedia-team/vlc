From: Vikram Kangotra <vikramkangotra8055@gmail.com>
Date: Sat, 3 Feb 2024 02:52:52 +0530
Subject: Taglib: Use ID3v2Tag() instead of tag() for RIFF::WAV::File

`WriteMetaToId3v2` expects a `ID3v2::Tag` instead of `Tag`, but Since TagLib v2.0,
`RIFF::WAV::File::tag()` returns a `Tag` instead of `ID3v2::Tag`, hence replace
the usage of `tag()` method with `ID3v2Tag()`.

https://github.com/taglib/taglib/blob/master/taglib/riff/wav/wavfile.h#L124

Additionally, to resolve the compilation error, the function signatures of
`insert` and `removeBlock` have been adjusted to align with the base class
functions.

(cherry picked from commit ec29dfca1e59530dd412d779e0b045079b72ffb6)
---
 modules/meta_engine/taglib.cpp | 40 ++++++++++++++++++++++++++++++++++++++++
 1 file changed, 40 insertions(+)

diff --git a/modules/meta_engine/taglib.cpp b/modules/meta_engine/taglib.cpp
index 487a152..0e7996e 100644
--- a/modules/meta_engine/taglib.cpp
+++ b/modules/meta_engine/taglib.cpp
@@ -137,7 +137,11 @@ VLCTagLib::ExtResolver<T>::ExtResolver(const std::string & ext)
 template <class T>
 File *VLCTagLib::ExtResolver<T>::createFile(FileName fileName, bool, AudioProperties::ReadStyle) const
 {
+#if defined(_WIN32) && TAGLIB_VERSION >= VERSION_INT(2, 0, 0)
+    std::string filename = fileName.toString().to8Bit(true);
+#else
     std::string filename = std::string(fileName);
+#endif
     std::size_t namesize = filename.size();
 
     if (namesize > ext.length())
@@ -155,7 +159,11 @@ File *VLCTagLib::ExtResolver<T>::createFile(FileName fileName, bool, AudioProper
 template<class T>
 File* VLCTagLib::ExtResolver<T>::createFileFromStream(IOStream* s, bool, AudioProperties::ReadStyle) const
 {
+#if defined(_WIN32) && TAGLIB_VERSION >= VERSION_INT(2, 0, 0)
+    std::string filename = s->name().toString().to8Bit(true);
+#else
     std::string filename = std::string(s->name());
+#endif
     std::size_t namesize = filename.size();
 
     if (namesize > ext.length())
@@ -215,7 +223,11 @@ public:
         return m_stream->psz_url;
     }
 
+#if TAGLIB_VERSION >= VERSION_INT(2, 0, 0)
+    ByteVector readBlock(size_t length)
+#else
     ByteVector readBlock(ulong length)
+#endif
     {
         if(m_borked || m_seqReadLength >= m_seqReadLimit)
             return {};
@@ -235,11 +247,19 @@ public:
         // Let's stay Read-Only for now
     }
 
+#if TAGLIB_VERSION >= VERSION_INT(2, 0, 0)
+    void insert(const ByteVector&, offset_t, size_t)
+#else
     void insert(const ByteVector&, ulong, ulong)
+#endif
     {
     }
 
+#if TAGLIB_VERSION >= VERSION_INT(2, 0, 0)
+    void removeBlock(offset_t, size_t)
+#else
     void removeBlock(ulong, ulong)
+#endif
     {
     }
 
@@ -258,7 +278,11 @@ public:
         m_seqReadLimit = s;
     }
 
+#if TAGLIB_VERSION >= VERSION_INT(2, 0, 0)
+    void seek(offset_t offset, Position p)
+#else
     void seek(long offset, Position p)
+#endif
     {
         uint64_t pos = 0;
         long len;
@@ -293,12 +317,20 @@ public:
         return;
     }
 
+#if TAGLIB_VERSION >= VERSION_INT(2, 0, 0)
+    offset_t tell() const
+#else
     long tell() const
+#endif
     {
         return m_previousPos;
     }
 
+#if TAGLIB_VERSION >= VERSION_INT(2, 0, 0)
+    offset_t length()
+#else
     long length()
+#endif
     {
         uint64_t i_size;
         if (vlc_stream_GetSize( m_stream, &i_size ) != VLC_SUCCESS)
@@ -306,7 +338,11 @@ public:
         return i_size;
     }
 
+#if TAGLIB_VERSION >= VERSION_INT(2, 0, 0)
+    void truncate(offset_t)
+#else
     void truncate(long)
+#endif
     {
     }
 
@@ -1425,7 +1461,11 @@ static int WriteMeta( vlc_object_t *p_this )
         if( RIFF::AIFF::File* riff_aiff = dynamic_cast<RIFF::AIFF::File*>(f.file()) )
             WriteMetaToId3v2( riff_aiff->tag(), p_item );
         else if( RIFF::WAV::File* riff_wav = dynamic_cast<RIFF::WAV::File*>(f.file()) )
+#if TAGLIB_VERSION >= VERSION_INT(2, 0, 0)
+            WriteMetaToId3v2( riff_wav->ID3v2Tag(), p_item );
+#else
             WriteMetaToId3v2( riff_wav->tag(), p_item );
+#endif
     }
     else if( TrueAudio::File* trueaudio = dynamic_cast<TrueAudio::File*>(f.file()) )
     {
